exports.up = function(knex) {
    return knex.schema.createTable('finanzas', (table) => {
        table.increments('id').primary();
        table.string('salario').notNullable();
        table.string('otrosingresos').notNullable();
        table.string('gastosmensuales').notNullable();
        table.string('gastosfinanzas').notNullable();
        table.integer('id_user').notNullable();
        table.foreign('id_user').references('usuario.id');
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('finanzas')
};