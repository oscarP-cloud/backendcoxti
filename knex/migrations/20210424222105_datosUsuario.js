exports.up = function(knex) {
    return knex.schema.createTable('usuario', (table) => {
        table.increments('id').primary();
        table.string('nombre').notNullable();
        table.string('celular').notNullable().unique();
        table.string('cedula').notNullable().unique();
        table.string('correo').notNullable().unique();
        table.string('contrasena').notNullable();
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('usuario')
};