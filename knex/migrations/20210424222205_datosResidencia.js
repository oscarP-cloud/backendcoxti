exports.up = function(knex) {
    return knex.schema.createTable('residencia', (table) => {
        table.increments('id').primary();
        table.string('departamento').notNullable();
        table.string('ciudad').notNullable();
        table.string('barrio').notNullable();
        table.string('direccion').notNullable();
        table.integer('id_user').notNullable();
        table.foreign('id_user').references('usuario.id');
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('residencia')
};