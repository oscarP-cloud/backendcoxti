Se crea el servicio con Nodejs, en este se usan las librerias pg(para usar posgrest), Knex(ORM), objection(para el modelado de BDs), express(establecer rutas) y body parser (conversion de datos a JSON).
En el archivo controller se encuentra las consultas y puntos que se consumen desde el frond, en el archivo prueban.js, se encuentra el scrip en donde al enviar un dato se obtiene las 3 posibles opciones en donde en 1 
se puede encontra el salario enviado.

Lo que mas se me dificulto fue la parte de consultas de las tablas relacionadas, por esa razón no realice la actividad de visualizar los datos cuando el usauario se registraba. Se implemento la libreria JWT para la generacion de tokens y usarse
para el logueo de usuario.

Nota: Para realizar las migraciones se debe colocar la terminal el comando knex migrate:latests. En el archvio knexfile, se encuentra configurado el nombre, puerto y base de datos que se usara.