'use strict';
const { Model } = require('objection');
const knex = require('../knex/knex.js');
Model.knex(knex);


class datosUsuario extends Model {
    static get tableName() {
        return 'usuario'
    }
}

module.exports = datosUsuario;