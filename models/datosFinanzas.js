'use strict';
const { Model } = require('objection');
const knex = require('../knex/knex.js');
Model.knex(knex);

class datosFinanzas extends Model {
    static get tableName() {
        return 'finanzas'
    }
    static relationMappings = {
        dependents: {
            relations: Model.HasManyRelation,
            modelClass: `${__dirname}/finanzas`,
            join: {
                from: 'finanzas.id_user',
                to: 'usuario.id'
            }
        }
    }
}
module.exports = datosFinanzas;