'use strict';
const { Model } = require('objection');
const knex = require('../knex/knex.js');
Model.knex(knex);

class datosResidencia extends Model {
    static get tableName() {
        return 'residencia'
    }
    static relationMappings = {
        dependents: {
            relations: Model.HasManyRelation,
            modelClass: `${__dirname}/usuario`,
            join: {
                from: 'residencia.id_user',
                to: 'usuario.id'
            }
        }
    }
}
module.exports = datosResidencia;