'use strict'

const app = require("./app");
const PORT = 1200;

app.listen(PORT, () => {
    console.log(" Server corriendo en el", PORT);
})

app.get('/', (req, res) => {
    res.send('Pagina de inicio');
})