'use strict'
var knex = require('../knex/knex');
const jwt = require('jsonwebtoken');
var usuario = require('../models/datosUsuario');
var residencia = require('../models/datosResidencia');
var finanzas = require('../models/datosFinanzas');
let numeros = [];
var valorsalario;
const JWT_Secret = "MeTOdoINgr3S0";
var controllers = {
    registro: async(req, res) => {
        var reqData = req.body;
        valorsalario = reqData.salario;
        try {
            await usuario.query().insert({
                nombre: reqData.nombre,
                celular: reqData.celular,
                cedula: reqData.cedula,
                correo: reqData.correo,
                contrasena: reqData.contrasena
            }).then((result) => {
                finanzas.query().insert({
                    salario: reqData.salario,
                    otrosingresos: reqData.otrosingresos,
                    gastosmensuales: reqData.gastosmensuales,
                    gastosfinanzas: reqData.gastosfinanzas,
                    id_user: result.id
                }).then((res) => {
                    residencia.query().insert({
                        departamento: reqData.departamento,
                        ciudad: reqData.ciudad,
                        barrio: reqData.barrio,
                        direccion: reqData.direccion,
                        id_user: res.id,
                    }).then((result) => {
                        res.status(200).send(result)
                    })
                })

            })
            res.status(200).send('Usuario registrado');

        } catch (err) {
            res.status(400).send(err)
        }

    },
    salario: (req, res) => {
        res.send(salario(valorsalario));
    },
    login: async(req, res) => {
        console.log(req.body);
        const reqData = req.body;
        try {
            await usuario.query()
                .select('*')
                .where('correo', '=', reqData.correo)
                .then((result) => {
                    console.log(result);
                    if (result[0].contrasena === reqData.contrasena) {
                        var token = jwt.sign(reqData, JWT_Secret);
                        res.status(200).send({
                            signed_user: reqData,
                            token: token
                        });
                        console.log('autenticado');
                    } else {
                        res.status(403).send({
                            errorMessage: 'Authorisation required!'
                        });
                    }
                })
        } catch (error) {
            console.log('Error de credenciales', error);
            res.status(400).send('Error de credenciales')
        }
    }
}


module.exports = controllers;