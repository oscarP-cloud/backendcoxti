'use strict'
const { Router } = require('express');
var express = require('express');
var Routes = express.Router();
var controllers = require('../controller/controller');

Routes.post('/api/registro', controllers.registro);
Routes.get('/api/salario', controllers.salario);
Routes.post('/api/login', controllers.login);

module.exports = Routes;